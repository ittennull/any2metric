FROM rust AS build
WORKDIR /src
COPY . .
RUN cargo build --release

FROM debian:bookworm-slim AS bin
WORKDIR /app
COPY --from=build /src/target/release/any2metric /src/log4rs.yaml ./
ENTRYPOINT ["./any2metric"]
