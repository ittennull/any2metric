use crate::configuration::MetricKind;
use crate::metric::Metric;
use anyhow::Result;
use log::error;
use prometheus_client::registry::Registry;

#[derive(Clone)]
pub struct SelfMetric {
    metric: Metric,
}

impl SelfMetric {
    pub fn new(app_name: String, registry: &mut Registry) -> Self {
        Self {
            metric: Metric::new(
                MetricKind::Counter,
                app_name.clone(),
                format!("{app_name} calls to configured sources"),
                registry,
            ),
        }
    }

    pub fn add_run_result(&self, metric_name: &str, result: Result<()>) {
        match result {
            Ok(()) => self.plus_one(&metric_name, "success").unwrap(),
            Err(err) => {
                self.plus_one(&metric_name, "failure").unwrap();
                error!("{metric_name} failed: {:#}", err)
            }
        }
    }

    fn plus_one(&self, metric_name: &str, result: &str) -> Result<()> {
        let labels = vec![
            ("metric".to_string(), metric_name.to_string()),
            ("result".to_string(), result.to_string()),
        ];

        self.metric.update(&labels, 1)
    }
}
