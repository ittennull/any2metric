use crate::configuration::MetricKind;
use anyhow::{anyhow, Result};
use prometheus_client::metrics::counter::Counter;
use prometheus_client::metrics::family::Family;
use prometheus_client::metrics::gauge::Gauge;
use prometheus_client::registry::Registry;

pub type Labels = Vec<(String, String)>;

#[derive(Clone)]
pub struct Metric {
    kind: MetricKind,
    gauge: Option<Family<Labels, Gauge>>,
    counter: Option<Family<Labels, Counter>>,
}

impl Metric {
    pub fn new(
        kind: MetricKind,
        name: String,
        description: String,
        registry: &mut Registry,
    ) -> Self {
        let mut metric = Self {
            kind,
            gauge: None,
            counter: None,
        };

        match metric.kind {
            MetricKind::Counter => {
                let counter = Family::default();
                registry.register(&name, &description, counter.clone());
                metric.counter = Some(counter);
            }
            MetricKind::Gauge => {
                let gauge = Family::default();
                registry.register(&name, &description, gauge.clone());
                metric.gauge = Some(gauge);
            }
        }

        metric
    }

    pub fn update(&self, labels: &Labels, value: i64) -> Result<()> {
        match self.kind {
            MetricKind::Counter => {
                if value < 0 {
                    return Err(anyhow!("Value is negative ({value}) but a metric type 'Counter' expects only positive increments"));
                }
                let guard = self.counter.as_ref().unwrap().get_or_create(labels);
                guard.inc_by(value as u64);
            }
            MetricKind::Gauge => {
                let guard = self.gauge.as_ref().unwrap().get_or_create(labels);
                guard.set(value);
            }
        };

        Ok(())
    }

    pub fn clear(&self) {
        match self.kind {
            MetricKind::Counter => {
                self.counter.as_ref().unwrap().clear();
            }
            MetricKind::Gauge => {
                self.gauge.as_ref().unwrap().clear();
            }
        };
    }
}
