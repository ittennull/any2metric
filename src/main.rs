use std::collections::{HashMap, HashSet};
use std::env;
use std::sync::Arc;

use anyhow::{anyhow, Context, Result};
use axum::extract::State;
use axum::http::{header, StatusCode};
use axum::response::IntoResponse;
use axum::routing::get;
use axum::Router;
use config::{Config, File};
use prometheus_client::encoding::text::encode;
use prometheus_client::registry::Registry;
use tokio_cron_scheduler::JobScheduler;

use crate::configuration::Configuration;
use crate::cronjob_settings::CronjobSettings;

mod configuration;
mod cronjob_settings;
mod cronjobs;
mod metric;
mod self_metric;

pub struct AppState {
    pub registry: Registry,
}

#[tokio::main]
async fn main() -> Result<()> {
    log4rs::init_file("log4rs.yaml", Default::default()).context("Loading ./log4rs.yaml")?;

    let config_filename = env::args().nth(1).unwrap_or("config.yaml".to_string());
    let (configuration, servers_to_connection_string) = load_configuration(&config_filename)?;

    let scheduler = JobScheduler::new().await?;
    let mut app_state = AppState {
        registry: Registry::default(),
    };

    let settings = CronjobSettings {
        app_name: env!("CARGO_PKG_NAME").to_string(),
        configuration,
        servers_to_connection_string,
    };
    cronjobs::create_cronjobs(settings, &scheduler, &mut app_state.registry).await?;

    scheduler.start().await?;

    let state = Arc::new(app_state);
    let app = Router::new()
        .route("/metrics", get(metrics_handler))
        .with_state(state);
    let listener = tokio::net::TcpListener::bind("0.0.0.0:8080").await?;
    axum::serve(listener, app).await?;

    Ok(())
}

async fn metrics_handler(State(state): State<Arc<AppState>>) -> impl IntoResponse {
    let mut body = String::new();
    encode(&mut body, &state.registry).unwrap();

    (
        StatusCode::OK,
        [(
            header::CONTENT_TYPE,
            "application/openmetrics-text; version=1.0.0; charset=utf-8",
        )],
        body,
    )
}

fn load_configuration(filename: &str) -> Result<(Configuration, HashMap<String, String>)> {
    let config = Config::builder()
        .add_source(File::with_name(filename))
        .add_source(config::Environment::default())
        .build()?;
    let configuration = config.clone().try_deserialize::<Configuration>()?;
    validate_configuration(&configuration)?;

    let servers_names: HashSet<&str> = configuration
        .job_configs
        .iter()
        .filter_map(|x| x.mongodb.as_ref())
        .map(|x| x.server.as_str())
        .collect();

    let mut servers = HashMap::with_capacity(servers_names.len());
    for server_name in servers_names.into_iter() {
        let env_name = format!("connection_string_{}", server_name.to_lowercase());
        let connection_string = config
            .get_string(&env_name)
            .map_err(|e| anyhow!("Environment variable {env_name} is not defined: {e}"))?;
        servers.insert(server_name.to_string(), connection_string);
    }

    Ok((configuration, servers))
}

fn validate_configuration(configuration: &Configuration) -> Result<()> {
    let both = configuration
        .job_configs
        .iter()
        .find(|x| x.mongodb.is_some() && x.http_request.is_some())
        .map(|x| &x.metric.name);
    if let Some(name) = both {
        return Err(anyhow!("Metric '{name}' defines multiple sources"));
    }

    let no_sources = configuration
        .job_configs
        .iter()
        .find(|x| x.mongodb.is_none() && x.http_request.is_none())
        .map(|x| &x.metric.name);
    if let Some(name) = no_sources {
        return Err(anyhow!("Metric '{name}' defines no sources"));
    }

    Ok(())
}
