use crate::configuration::{HttpRequestSource, JobConfig, MetricConfig, MetricKind};
use crate::metric::{Labels, Metric};
use anyhow::{anyhow, Context, Result};
use log::info;
use std::sync::Arc;
use std::time::Duration;

use crate::self_metric::SelfMetric;
use serde_json::{Map, Value};
use tokio_cron_scheduler::{Job, JobScheduler};

pub async fn create_cronjob(
    job_cfg: JobConfig,
    sched: &JobScheduler,
    metric: Metric,
    self_metric: SelfMetric,
) -> Result<()> {
    let data = Arc::new((
        metric,
        self_metric,
        job_cfg.metric,
        job_cfg.http_request.expect("Must be a http_request source"),
    ));

    sched
        .add(Job::new_async(job_cfg.cron.as_str(), move |_, _| {
            let data = data.clone();

            Box::pin(async move {
                let (metric, self_metric, metric_cfg, http_request) = &*data;

                let result = perform_http_request(metric_cfg, http_request, metric).await;

                self_metric.add_run_result(&metric_cfg.name, result);
            })
        })?)
        .await?;

    Ok(())
}

async fn perform_http_request(
    metric_cfg: &MetricConfig,
    http_request_source: &HttpRequestSource,
    metric: &Metric,
) -> Result<()> {
    // clear gauge up, it will be fully repopulated by the results of a http request
    // it saves me from remembering labels and setting the corresponding values to zero when the query returns no documents for a particular label
    if metric_cfg.kind == MetricKind::Gauge {
        metric.clear();
    }

    let json: Value = {
        let client = reqwest::Client::new();
        let timeout = http_request_source
            .timeout
            .map(Duration::from_secs)
            .unwrap_or(Duration::from_secs(30));
        let request_builder = client.get(&http_request_source.url).timeout(timeout);
        request_builder.send().await?.json().await?
    };

    info!("{}: HTTP response: {}", metric_cfg.name, json);

    match json {
        Value::Null | Value::Bool(_) | Value::Number(_) | Value::String(_) => {
            return Err(anyhow!(
                "{}: URL {} must return an object or an array of objects",
                metric_cfg.name,
                http_request_source.url
            ));
        }

        Value::Array(array) => {
            for obj in array {
                match obj {
                    Value::Object(obj) => update_metric_value(
                        metric,
                        metric_cfg,
                        http_request_source.fields_as_labels.as_ref(),
                        obj,
                    )?,
                    _ => {
                        return Err(anyhow!(
                            "{}: Array from response must contain only objects",
                            metric_cfg.name
                        ));
                    }
                }
            }
        }

        Value::Object(obj) => update_metric_value(
            metric,
            metric_cfg,
            http_request_source.fields_as_labels.as_ref(),
            obj,
        )?,
    }

    Ok(())
}

fn update_metric_value(
    metric: &Metric,
    metric_cfg: &MetricConfig,
    fields_as_labels: Option<&Vec<String>>,
    mut object: Map<String, Value>,
) -> Result<()> {
    let field_with_update_value = match metric_cfg.kind {
        MetricKind::Counter => &metric_cfg.increment_counter_by_value_of_field,
        MetricKind::Gauge => &metric_cfg.set_gauge_to_value_of_field,
    };

    let update_value = match field_with_update_value {
        None => 1,
        Some(field) => object
            .remove(field)
            .ok_or_else(|| anyhow!("Document doesn't have field `{field}`"))
            .map(|x| {
                x.as_i64().with_context(|| {
                    anyhow!("Can't convert the value of field `{field}` to an integer")
                })
            })??,
    };

    let labels = to_labels(object, fields_as_labels);

    metric.update(&labels, update_value)
}

fn to_labels(object: Map<String, Value>, fields_as_labels: Option<&Vec<String>>) -> Labels {
    object
        .into_iter()
        .filter(|(name, _)| match fields_as_labels {
            None => true,
            Some(fields) => fields.contains(name),
        })
        .map(|(name, value)| {
            let val = match &value {
                Value::String(x) => x.to_owned(),
                x => x.to_string(),
            };
            (name, val)
        })
        .collect()
}
