use anyhow::{anyhow, Result};
use prometheus_client::registry::Registry;
use tokio_cron_scheduler::JobScheduler;

use crate::cronjob_settings::CronjobSettings;
use crate::metric::Metric;
use crate::self_metric::SelfMetric;

mod http_request;
mod mongodb;
mod query_parser;

pub async fn create_cronjobs(
    settings: CronjobSettings,
    sched: &JobScheduler,
    registry: &mut Registry,
) -> Result<()> {
    let mongo_clients =
        mongodb::create_mongodb_clients(&settings.app_name, settings.servers_to_connection_string)
            .await?;

    let self_metric = SelfMetric::new(settings.app_name.clone(), registry);

    for job_cfg in settings.configuration.job_configs {
        let metric = Metric::new(
            job_cfg.metric.kind.clone(),
            job_cfg.metric.name.clone(),
            job_cfg.metric.description.clone(),
            registry,
        );

        if let Some(ref mongodb) = job_cfg.mongodb {
            let client = mongo_clients[&mongodb.server].clone();
            mongodb::create_cronjob(
                &settings.app_name,
                client,
                job_cfg,
                sched,
                metric,
                self_metric.clone(),
            )
            .await?;
        } else if let Some(_) = job_cfg.http_request {
            http_request::create_cronjob(job_cfg, sched, metric, self_metric.clone()).await?;
        } else {
            return Err(anyhow!("None of the supported sources were found"));
        }
    }

    Ok(())
}
