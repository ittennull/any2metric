use anyhow::Result;
use anyhow::{anyhow, Context};
use boa_engine::builtins::date::Date;
use boa_engine::builtins::object::OrdinaryObject;
use boa_engine::object::builtins::JsDate;
use boa_engine::property::{NonMaxU32, PropertyKey};
use boa_engine::{JsValue, Source};
use bson::Bson;
use bson::Bson::{Array, Document};

const PRELUDE: &str = r#"
function ISODate(e) {
    e || (e = (new Date).toISOString()); 
    const t = e.match(/^(?<Y>\d{4})-?(?<M>\d{2})-?(?<D>\d{2})([T ](?<h>\d{2})(:?(?<m>\d{2})(:?((?<s>\d{2})(\.(?<ms>\d+))?))?)?(?<tz>Z|([+-])(\d{2}):?(\d{2})?)?)?$/); 
    if (null !== t && void 0 !== t.groups) { 
        const { Y: e, M: r, D: n, h: i, m: s, s: o, ms: a, tz: l } = t.groups, 
            c = new Date(`${e}-${r}-${n}T${i || "00"}:${s || "00"}:${o || "00"}.${a || "000"}${l || "Z"}`); 
        
        if (c.getTime() >= -621672192e5 && c.getTime() <= 0xe677d21fdbff) 
            return c 
    } 
    throw new a.MongoshInvalidInputError(`${JSON.stringify(e)} is not a valid ISODate`, a.CommonErrors.InvalidArgument)
}

"#;

pub fn parse_query(query: &str) -> Result<Vec<bson::Document>> {
    let mut context = boa_engine::Context::default();

    // load prelude with common helper functions
    context
        .eval(Source::from_bytes(PRELUDE))
        .map_err(|err| anyhow!("Couldn't eval PRELUDE: {err}"))?;

    // load the query and convert its JS output to an array of BSON documents
    match context.eval(Source::from_bytes(query)) {
        Ok(res) => {
            convert_jsvalue(res, &mut context).context(
                "See https://www.mongodb.com/docs/manual/reference/method/db.collection.aggregate/ to get examples of input documents for aggregate function"
            )
        }
        Err(e) => Err(anyhow!("Failed to parse query: {e}")),
    }
}

fn convert_jsvalue(
    jsvalue: JsValue,
    context: &mut boa_engine::Context,
) -> Result<Vec<bson::Document>> {
    let converted_value =
        convert(&jsvalue, context).map_err(|err| anyhow!("Converting failed: {err}"))?;
    let array = converted_value
        .as_array()
        .ok_or(anyhow!("Query is not an array"))?;

    let mut result = vec![];
    for bson in array {
        let doc = bson
            .as_document()
            .ok_or(anyhow!("Query must define an array of documents"))?;
        result.push(doc.clone());
    }

    Ok(result)
}

fn convert(jsvalue: &JsValue, context: &mut boa_engine::Context) -> Result<Bson> {
    match jsvalue {
        JsValue::Null => Ok(Bson::Null),
        JsValue::Boolean(x) => Ok(Bson::Boolean(*x)),
        JsValue::String(x) => Ok(Bson::String(x.to_std_string()?)),
        JsValue::Rational(x) => Ok(Bson::Double(*x)),
        JsValue::Integer(x) => Ok(Bson::Int32(*x)),
        JsValue::BigInt(x) => Ok(Bson::Double(x.to_f64())),

        JsValue::Object(js_object) if js_object.is::<Date>() => {
            let milliseconds = JsDate::from_object(js_object.clone())
                .map_err(|err| anyhow!("Failed to convert date: {err}"))?
                .get_time(context)
                .map_err(|err| anyhow!("get_time function failed: {err}"))?
                .as_number()
                .ok_or(anyhow!("get_time returned not a number"))?
                as i64;
            Ok(Bson::DateTime(bson::DateTime::from_millis(milliseconds)))
        }

        JsValue::Object(js_object) if js_object.is_array() => {
            let array_values = OrdinaryObject::values(jsvalue, &[jsvalue.clone()], context)
                .map_err(|err| anyhow!("Failed to get array values: {err}"))?;
            let array = array_values
                .as_object()
                .ok_or(anyhow!("Expected to get an object from Object::values"))?
                .borrow();

            let mut bson_array = bson::Array::new();
            for i in array.properties().index_property_keys() {
                let field_value = js_object
                    .get(PropertyKey::Index(NonMaxU32::new(i).unwrap()), context)
                    .map_err(|err| anyhow!("Failed to get an array value by index: {err}"))?;
                let bson_value = convert(&field_value, context)?;
                bson_array.push(bson_value);
            }

            Ok(Array(bson_array))
        }

        JsValue::Object(js_object) => {
            let names =
                OrdinaryObject::get_own_property_names(jsvalue, &[jsvalue.clone()], context)
                    .map_err(|err| anyhow!("Failed to get property names for an object: {err}"))?;
            let array = names
                .as_object()
                .ok_or(anyhow!(
                    "Expected to get an object from Object::get_own_property_names"
                ))?
                .borrow();

            let mut bson_doc = bson::Document::new();
            for property_descriptor in array.properties().index_property_values() {
                let field_name = property_descriptor
                    .value()
                    .ok_or(anyhow!("Failed to get a value from property_descriptor"))?
                    .as_string()
                    .ok_or(anyhow!("Expected to get a string from property_descriptor"))?;
                let field_value = js_object
                    .get(PropertyKey::String(field_name.clone()), context)
                    .map_err(|err| anyhow!("Failed to get an object value by key: {err}"))?;
                let bson_value = convert(&field_value, context)?;
                bson_doc.insert(field_name.to_std_string()?, bson_value);
            }

            Ok(Document(bson_doc))
        }
        x => Err(anyhow!("Unsupported type {}", x.type_of())),
    }
}
