use super::query_parser::parse_query;
use crate::configuration::{JobConfig, MetricConfig, MetricKind, MongoDbSource};
use crate::metric::Metric;
use crate::self_metric::SelfMetric;
use anyhow::{anyhow, Context, Result};
use bson::{Bson, Document};
use futures::TryStreamExt;
use log::info;
use mongodb::options::ClientOptions;
use mongodb::Client;
use std::collections::HashMap;
use std::sync::Arc;
use tokio_cron_scheduler::{Job, JobScheduler};

pub async fn create_cronjob(
    app_name: &str,
    mongodb_client: Client,
    job_cfg: JobConfig,
    sched: &JobScheduler,
    metric: Metric,
    self_metric: SelfMetric,
) -> Result<()> {
    let data = Arc::new((
        app_name.to_string(),
        metric,
        self_metric,
        job_cfg.metric,
        job_cfg.mongodb.expect("Must be a mongodb source"),
        mongodb_client,
    ));

    sched
        .add(Job::new_async(job_cfg.cron.as_str(), move |_, _| {
            let data = data.clone();

            Box::pin(async move {
                let (app_name, metric, self_metric, metric_cfg, mongodb_source, mongodb_client) =
                    &*data;

                let result =
                    run_query(app_name, mongodb_client, metric_cfg, mongodb_source, metric).await;

                self_metric.add_run_result(&metric_cfg.name, result);
            })
        })?)
        .await?;

    Ok(())
}

async fn run_query(
    app_name: &str,
    mongodb_client: &Client,
    metric_cfg: &MetricConfig,
    mongodb: &MongoDbSource,
    metric: &Metric,
) -> Result<()> {
    let db = mongodb_client.database(&mongodb.database);
    let collection = db.collection::<Document>(&mongodb.collection);

    let query = parse_query(&mongodb.query)?;

    let mut cursor = collection
        .aggregate(query)
        .comment(format!(
            "{app_name}: running a query for metric {}",
            metric_cfg.name
        ))
        .await?;

    // clear gauge up, it will be fully repopulated by the results of a database query.
    // it saves me from remembering labels and setting the corresponding values to zero when the query returns no documents for a particular label
    if metric_cfg.kind == MetricKind::Gauge {
        metric.clear();
    }

    let mut doc_count = 0;
    while let Some(doc) = cursor.try_next().await? {
        info!(
            "{}: query result {}: {:?}",
            metric_cfg.name,
            doc_count + 1,
            doc
        );

        update_metric_value(metric, metric_cfg, mongodb.fields_as_labels.as_ref(), doc)?;

        doc_count += 1;
    }

    info!(
        "{}: Query returned {} documents",
        metric_cfg.name, doc_count
    );

    Ok(())
}

fn update_metric_value(
    metric: &Metric,
    metric_cfg: &MetricConfig,
    fields_as_labels: Option<&Vec<String>>,
    mut doc: Document,
) -> Result<()> {
    let field_with_update_value = match metric_cfg.kind {
        MetricKind::Counter => &metric_cfg.increment_counter_by_value_of_field,
        MetricKind::Gauge => &metric_cfg.set_gauge_to_value_of_field,
    };

    let update_value = match field_with_update_value {
        None => 1,
        Some(field) => doc
            .remove(field)
            .with_context(|| anyhow!("Document doesn't have field `{field}`"))?
            .as_i32()
            .with_context(|| anyhow!("Can't parse field `{field}` as an integer"))?
            as i64,
    };

    let labels = doc
        .into_iter()
        .filter(|(name, _)| match fields_as_labels {
            None => true,
            Some(fields) => fields.contains(name),
        })
        .map(|(key, value)| {
            let value = match value {
                Bson::String(s) => s,
                x => x.to_string(),
            };
            (key, value)
        })
        .collect();

    metric.update(&labels, update_value)
}

pub async fn create_mongodb_clients(
    app_name: &str,
    servers_to_connection_string: HashMap<String, String>,
) -> Result<HashMap<String, Client>> {
    let mut mongo_clients = HashMap::default();
    for (name, connection_string) in servers_to_connection_string {
        let mut client_options = ClientOptions::parse(&connection_string).await?;
        client_options.app_name = Some(app_name.to_string());

        let client = Client::with_options(client_options)?;
        mongo_clients.insert(name.clone(), client);
    }

    Ok(mongo_clients)
}
