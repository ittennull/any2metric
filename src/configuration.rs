use serde::Deserialize;

#[derive(Deserialize)]
pub struct Configuration {
    pub job_configs: Vec<JobConfig>,
}

#[derive(Deserialize)]
pub struct JobConfig {
    pub metric: MetricConfig,
    pub cron: String,
    pub mongodb: Option<MongoDbSource>,
    pub http_request: Option<HttpRequestSource>,
}

#[derive(Deserialize)]
pub struct MongoDbSource {
    pub server: String,
    pub database: String,
    pub collection: String,
    pub query: String,
    pub fields_as_labels: Option<Vec<String>>,
}

#[derive(Deserialize)]
pub struct HttpRequestSource {
    pub url: String,
    pub fields_as_labels: Option<Vec<String>>,
    pub timeout: Option<u64>, // in seconds
}

#[derive(Deserialize)]
pub struct MetricConfig {
    pub name: String,
    pub description: String,
    pub kind: MetricKind,
    pub increment_counter_by_value_of_field: Option<String>,
    pub set_gauge_to_value_of_field: Option<String>,
}

#[derive(Deserialize, Clone, PartialEq)]
pub enum MetricKind {
    Counter,
    Gauge,
}
