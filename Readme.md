**any2metric** converts data to prometheus metrics and exposes them on `/metrics` endpoint. The data is taken from
sources. There are currently the following supported sources:

- MongoDb queries
- HTTP requests

## Motivation

Sometimes it's necessary to have some heavy database queries that for example check integrity of the data. While
it's possible to run a query right after a data modification, it brings a considerable load on the database. Moreover,
it is not always needed to run the query so often, it could be once per hour or daily

Querying alone isn't much useful, the query results can tell you more once they are plotted on a chart. On top of that,
an alerting can prove to be a life savior.

While Grafana handles visualization well and Alertmanager takes care of alerts, I lacked means to query a database and
make metrics out of the results

This project fills this gap without trying to reinvent the rest.

HTTP source is especially helpful with 3rd party services that you have no control of, but they happen to have an API
that returns the needed data

## How to run locally

Create a file [config.yaml](config.yaml) with metric definitions, then run this

```shell
docker run --rm -it \
  -e "CONNECTION_STRING_MYSERVER=mongodb://localhost/?directConnection=true" \
  -v ./config.yaml:/app/config.yaml \
  -p 8080:8080 \
  registry.gitlab.com/ittennull/any2metric:main
```

If `config.yaml` has several values for `server` key, all the connection strings must be added to
the `docker run -e ...` arguments

## How to use in kubernetes

1. Deploy a container **any2metric**
2. Provide configuration in `config.yaml` file and put it to a _configmap_ that is used by the pod as a file
2. Set up
   a [PodMonitor](https://github.com/prometheus-operator/prometheus-operator/blob/main/Documentation/api.md#monitoring.coreos.com/v1.PodMonitor)
   to pull the metrics
3. Set up dashboards and alerting (outside of the scope of this document)

You can find an example in folder [k8s-deployment-example](./k8s-deployment-example)

Replace the configuration in `configmap.yml`, change the secret value in `secret.yml` and run the command

```shell
kubectl apply -n <your-namespace> -f ./k8s-deployment-example
```

### HTTP source

**any2metric** calls provided `url` with a `GET` request. The expected result is JSON. It can be a single object or an
array of objects. In the later case the metric is set/updated for every object of the array.

For example, a metric is defined as

```yaml
- metric:
    name: my_metric_name
    description: Something important
    kind: Gauge
    set_gauge_to_value_of_field: value
  cron: "*/2 * * * * * *"
  http_request:
    url: "http://example.com/data"
```

Let's say `http://example.com/data` returns this response

```json
[
  {
    "name": "abc",
    "value": 12,
    "data": "data1"
  },
  {
    "name": "xyz",
    "value": 45,
    "data": "data2"
  }
]
```

Then there will be a metric exposed on the `/metrics` endpoint with following labels and values:

```
# HELP my_metric_name Something important
# TYPE my_metric_name gauge
my_metric_name{name="abc",data="data1"} 12
my_metric_name{name="xyz",data="data2"} 45
```